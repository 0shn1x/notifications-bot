# Notifications Bot 
[![Build Status](https://travis-ci.com/oshnix/notifications-bot.svg?branch=master)](https://travis-ci.org/oshnix/notifications-bot.svg?branch=master)
![Nodejs version](https://img.shields.io/badge/node-%3E%3D10-brightgreen.svg)
![GitHub](https://img.shields.io/github/license/mashape/apistatus.svg)

## Info
This bot is currently under development for educational purposes only   
Feel free to use contribute or copy sources
