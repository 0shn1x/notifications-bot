const path = require('path');
nodeExternals = require('webpack-node-externals');

module.exports = {
	entry: {
		app: './src/index.js'
	},
	target: 'node',
	output: {
		filename: 'server.js',
		path: path.resolve(__dirname, '../dist/')
	},
	externals: [nodeExternals()],
	resolve: {
		alias: {
			Store: path.resolve(__dirname, '../src/store'),
			Database: path.resolve(__dirname, '../src/database'),
			VK: path.resolve(__dirname, '../src/vk'),
			Server: path.resolve(__dirname, '../src/server'),
		}
	},
	optimization: {
		noEmitOnErrors: true
	},
	module: {
		rules: [
			{
				enforce: 'pre',
				test: /\.js$/,
				exclude: /node_modules/,
				loader: 'eslint-loader',
				options: {
					fix: true,
					failOnError: true
				}
			}
		]
	}
};