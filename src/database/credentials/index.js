import { schema } from './credentials.schema';
import { createModel } from 'Database/util';

const credentialsModel = createModel('Credential', schema, 'credentials');

export async function getBotCredentials (conditions) {
	let searchResult = await credentialsModel.findOne(Object.assign({ type: 'bot' }, conditions));
	return searchResult['data'];
}
