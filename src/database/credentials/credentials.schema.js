import mongoose from 'mongoose';

const availableAPIs = ['vk'];
const availableTypes = ['bot'];

export const schema = new mongoose.Schema({
	type: {
		type: String,
		enum: availableTypes
	},
	api: {
		type: String,
		enum: availableAPIs
	},
	data: mongoose.Schema.Types.Mixed
});
