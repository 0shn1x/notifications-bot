import mongoose from 'mongoose';

export function connect (databaseName) {
	const HOST = process.env.HOST || 'localhost';
	const PORT = process.env.DB_PORT || 27017;
	return mongoose.connect(`mongodb://${HOST}:${PORT}/${databaseName}`, { useNewUrlParser: true });
}
