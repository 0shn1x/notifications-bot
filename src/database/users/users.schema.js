import mongoose from 'mongoose';

export const schema = new mongoose.Schema({
	name: {
		first: {
			type: String,
			required: true
		},
		last: {
			type: String,
			required: true
		},
	},
	dativeName: {
		first: {
			type: String,
			required: true
		},
		last: {
			type: String,
			required: true
		},
	},
	vk_id: {
		type: Number
	}
});

schema.virtual('fullName').get(() => {
	return `${this.name.first} + ${this.name.last}`;
});

schema.virtual('fullDativeName').get(() => {
	return `${this.dativeName.first} + ${this.dativeName.last}`;
});
