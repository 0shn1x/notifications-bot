import mongoose from 'mongoose';

export function createModel (modelName, schema, collectionName) {
	let params = [modelName, schema];
	// If collection name is absent, mongoose will generate it from modal name
	if (collectionName) {
		params.push(collectionName);
	}
	return mongoose.model(...params);
}
