import http from 'http';
import vk_handler from './vk_handler';

function getData (request) {
	let data = '';
	return new Promise((resolve, reject) => {
		request.on('data', dataPart => data += dataPart);
		request.on('end', () => resolve(data));
		request.on('aborted', () => reject('Request was aborted'));
	});
}

// Should process only POST requests
// TODO move requests filter to Nginx
export let createServer = http.createServer.bind(http, async (request, response) => {
	if (request.method !== 'POST') {
		response.writeHead(405);
		response.end();
		return;
	}
	let data;
	try {
		data = await getData(request);
	} catch (error) {
		console.error(error);
		if (!response.connection.destroyed) {
			response.writeHead(400);
			response.end();
		}
	}
	// When not only vk is implemented than choose module to process request
	let [code, message] = vk_handler(data);
	response.writeHead(code);
	// If message is undefined nothing will be printed
	response.end(message);
});
