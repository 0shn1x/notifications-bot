import { processRequestData } from 'VK/request';
import codes from './responseCodes';


export default data => {
	let payload;
	try {
		payload = JSON.parse(data);
	} catch (error) {
		return [codes.BAD_REQUEST];
	}
	if (!('type' in payload && 'secret' in payload && 'group_id' in payload)) {
		return [codes.FORBIDDEN];
	}
	return processRequestData(payload);
};