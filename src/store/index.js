function setter (oldValue, newValue) {
	Object.assign(oldValue, newValue);
}

function getter (oldValue) {
	return oldValue;
}
// move vk to separate module
const vk = {};
let vk_credentials = {};
Object.defineProperties(vk, {
	credentials: {
		set: setter.bind(vk, vk_credentials),
		get: getter.bind(vk, vk_credentials)
	}
});

const storage = {
	vk
};

export default storage;


