import * as bot from 'VK/bot';
import store from 'Store';

const TYPE = true;

const availableActions = {
	'help': 'Пока не реализовано',
	'привет': 'И тебе привет',
	'пока': 'Пока-пока!',
	'тест': 'Работает исправно!'
};

function sendMessage (user_id, message) {
	return bot.sendMethodRequest('messages.send', {
		user_id,
		message
	});
}

export default async message_data => {
	let promises = [];
	const { from_id, text } = message_data;
	// Typing message appears only after 4 seconds
	let delay = 6000, type = TYPE;

	const message = text.replace(/\r|\r\n|\n/, ' ').split(' ');
	const action = message[0].toLocaleLowerCase();

	if (availableActions.hasOwnProperty(action)) {
		bot.sendMethodRequest('messages.markAsRead', {
			peer_id: from_id,
			start_message_id: message_data['id']
		});
		promises.push(Promise.resolve(availableActions[action]));
	} else {
		promises.push(Promise.resolve('Я не понимаю'));
		type = false;
	}

	if (type) {
		promises.push(bot.sendMethodRequest('messages.setActivity', {
			user_id: from_id,
			type: 'typing',
			group_id: store.vk.credentials['group_id']
		}).then(() => {
			return new Promise(resolve => {
				setTimeout(resolve, delay);
			});
		}));
	}

	Promise.all(promises).then(res => {
		let answer = res[0];
		sendMessage(from_id, answer);
	});
};
