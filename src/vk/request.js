import store from 'Store';
import vk_methods from './request_methods';
import codes from 'Server/responseCodes';

// TODO add DDOS protection and Nginx proxy
export function processRequestData (payload) {
	if (payload.secret !== store.vk.credentials.secret) {
		return [codes.NOT_AUTHORIZED];
	}
	let handlerResult = vk_methods[payload.type](payload.object);
	let result = [codes.OK];
	result.push(typeof handlerResult === 'string' ? handlerResult : 'ok');
	return result;
}
