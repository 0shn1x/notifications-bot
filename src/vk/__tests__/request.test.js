jest.mock('Store');
jest.mock('VK/message', () => jest.fn().mockImplementation(() => {
	console.error('Hello from mock');
}));

const { processRequestData } = require('../request');

describe('VK requests processing', () => {
	it('Should process confirmation request', () => {
		let request = {
			secret: 'secret',
			type: 'confirmation'
		};
		expect(processRequestData(request)).toEqual([200, 'confirmation']);
	});

	it('Should deny incorrect secret', () => {
		let request = {
			secret: 'wrong_secret',
			type: 'confirmation'
		};
		expect(processRequestData(request)).toEqual([401]);
	});

	it('Actual "message_new" handler should be mocked', () => {
		const spy = jest.spyOn(console, 'error');
		const method = 'message_new';
		let request = {
			secret: 'secret',
			type: method
		};
		expect(processRequestData(request)).toEqual([200, 'ok']);
		expect(spy).toHaveBeenCalledTimes(1);
		expect(spy).toBeCalledWith(`Hello from mock`);
		spy.mockRestore();
	});

	it('Should return ok to non-existing method and log to console', () => {
		const spy = jest.spyOn(console, 'error');
		const method = 'method_that_does_not_exists';
		let request = {
			secret: 'secret',
			type: method
		};
		expect(processRequestData(request)).toEqual([200, 'ok']);
		expect(spy).toHaveBeenCalledTimes(1);
		expect(spy).toBeCalledWith(`No such method ${method}`);
		spy.mockRestore();
	});
});
