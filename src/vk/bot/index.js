import https from 'https';
import {URL} from 'url';
import store from 'Store';

const baseUrl = 'https://api.vk.com';
const version = '5.80';

export function sendMethodRequest (methodName, queryParams = {}) {
	const url = new URL(`${baseUrl}/method/${methodName}`);
	for (let paramName in queryParams) {
		if (queryParams.hasOwnProperty(paramName)) {
			url.searchParams.append(paramName, queryParams[paramName]);
		}
	}
	url.searchParams.append('access_token', store.vk.credentials['token']);
	url.searchParams.append('v', version);

	return new Promise((resolve, reject) => {
		let req = https.get(url, res => {
			if (res.statusCode < 200 || res.statusCode >= 300) {
				return reject(new Error('statusCode=' + res.statusCode));
			}

			let data = '';
			res.on('data', dataPart => {
				data += dataPart;
			});

			res.on('end', () => {
				try {
					resolve(JSON.parse(data).response);
				} catch(e) {
					reject(e);
				}
			});
		});
		req.on('error', err => {
			reject(err);
		});
		req.end();
	});
}
