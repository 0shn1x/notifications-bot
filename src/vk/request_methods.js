import store from 'Store';
import messageHandler from 'VK/message';

const vk_methods = {
	confirmation () {
		return store.vk.credentials.confirmation;
	},
	'message_new': messageHandler
};

export default new Proxy(vk_methods, {
	get: (target, method) => {
		if (!target.hasOwnProperty(method)) {
			//TODO log incident to console
			console.error(`No such method ${method}`);
			return () => 'ok';
		}
		return target[method];
	}
});
