# Common app spec
## Server info
Application use node.js embedded http module to serve simple server and process incoming requests.
## Startup
Application should perform number of tasks in order to prepare environment    
If any of this steps fails - error should be logged and application should shut down.    
PM2 watcher should reboot application.
1. Connect to Mongo database
1. Fetch all required data from database
   * Bot credentials for all systems (only VK available at the moment).
   * Allowed users credentials. Map should be created in order to get user's info in O(1).
1. Start server application and log used port
## Request process
1. Make sure that request is valid
2. Detect system that sent this request
3. Pass request data to this system's handler.
