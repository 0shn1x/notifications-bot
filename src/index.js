// import { install } from 'source-map-support';
import { connect } from 'Database/connector';
import { getBotCredentials } from 'Database/credentials';
import { createServer } from './server';
import store from 'Store';

// install();
const PORT = process.env.PORT || 5038;
const server = createServer();

connect('app').then(() => {
	return Promise.all([
		getBotCredentials({ api: 'vk' }),
	]);
}).then(([vkBotInfo]) => {
	store.vk.credentials = vkBotInfo;
	console.info(`Listening on port ${PORT}.`);
	server.listen(PORT);
});
