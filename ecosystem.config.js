module.exports = {
	apps: {
		name: 'bot',
		script: './dist/server.js',
		watch: ['./dist'],
		exec_interpreter: 'node',
		log_date_format : "YY-MM-DD HH:mm Z"
	}
};
